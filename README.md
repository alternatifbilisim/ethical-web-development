# Guide for Ethical Web Development

**Guide for Ethical Web Development** is a booklet which aims
to help web developers how they can improve their works from
an ethical perspective. It was first published in January 2020
by [EDRI](https://edri.org).

Original work: https://edri.org/our-work/ethical-web-dev-2/

It was translated into Turkish by Alternatif Bilisim in
November 2020.

This repo is a markdown book version of the guide and we,
developers, are working together and trying to keep
the guide updated.

For now only English and Turkish languages are available.
You can see latest versions here:

- English: https://ethicalweb.alternatifbilisim.org/en/
- Turkish: https://ethicalweb.alternatifbilisim.org/tr/


## How can I contribute
We are using [mdbook](http://rust-lang.github.io/mdBook/) to build
our booklet. Please follow the instructions on that page to install. 

If you want to translate into a new language, please clone
directory `english` and rename it. Change markdown files under
`src/` directory.

If you want to contribute existing content (fix, improve, add),
change markdown file under `src` folder. 

To build, you can run following command inside new directory:


```
$ cp -rf english new-lang
$ cd new-lang
$ mdbook build
```

After building you can spin an http server in `book` directory:

```
cd book
python -m http.server 8100
```

Now you can see updated book by navigating to
http://localhost:8100/ with your browser.

Please do not forget to create merge request. Once it approved,
it will be published automatically by CI.

Please create an issue for your questions or any
other discussions.
