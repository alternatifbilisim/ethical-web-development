# Summary

[Front](./front.md)
[Preface](./preface.md)

- [0. Introduction](./0-introduction.md)
- [1. The Basics](./1-the-basics.md)
- [2. General Recommendations](./2-general-recommendations.md)
- [3. Security Recommendations](./3-security-recommendations.md)
- [4. Alternatives to costly “free” third-party services](./4.alternatives-to-costly-free-third-party-services.md)
- [5. Glossary](./5-glossary.md)

[Edri](./edri.md)
[Back](./back.md)
