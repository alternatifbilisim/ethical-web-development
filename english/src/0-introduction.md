# 0. Introduction

A website is almost like a living thing. Most of the time the basic site itself is not static and in addition to its own dynamic features, its environment is also subject to continuous change which in turn leads to even more changes.

Visitors of a website can also be very diverse. The technologies they use and their expertise may vary widely.

Many websites themselves also rely on a variety of external services and resources. These also continue to evolve.

As website developers have to cope at the same time with the increasing expectations of users and the limited resources most organisations devote to website development, there is a growing tendency to use more external services and resources.

For example, it has become more and more common for web developers to take “free” resources, such as fonts and scripts and use them on the websites that they design. While these are “free” for the developer, they can have undesirable side effects for the users and the organisations that provide the website. For example some resources and services, particularly those provided by certain data hungry internet companies, can undermine user privacy. Others can have adverse affects on security. In both cases, the reputation of the website owner may suffer, or it may even face legal challenges.

This warrants attention. However, there is a general lack of awareness of this problem, and these practices have already become quite pervasive. The purpose of this document is to clarify the problems and, where possible, identify some usable solutions.

This guide is aimed at web developers and maintainers who have a strong understanding of technical concepts. Links are provided to background information, where necessary, in order to keep the document brief and maximise its usability.

We hope that this will assist developers and maintainers in bringing the web back to its roots – a decentralised tool that can enhance fundamental rights, democracy and freedom of expression.

It is also our internet. We all need to take responsibility.
