# European Digital Rights

![Edri Logo](./edri.jpg)

Founded in 2002, EDRi is the biggest European network defending rights and freedoms online.

Currently 42 non-governmental organisations are members of EDRi and 30 observers closely contribute to our work.

Our mission is to promote, protect and uphold human rights and the rule of law in the digital environment, including the right to privacy, data protection, and freedom of expression and information.

Our vision is for a Europe where state authorities and private companies respect everyone’s fundamental rights and freedoms in the online environment. Our overall aim is to build the structures where civil society and individuals are empowered to embrace technological progress in control of their rights.


[![Edri Donate](./donate.jpg)](https://edri.org/donate)