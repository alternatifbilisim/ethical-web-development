![Security is actually quite simple, cleaning up after a breach is complicated. Your website visitors deserve respect. This means not taking shortcuts that lead to their data being leaked to third parties and it means ensuring adequate security. Respecting your visitors also means taking a big step towards compliance with data protection legislation.](./preface-respect.jpg)


----------------


*This publication is a result of an extensive collective work, with inputs from experts of the EDRi network (Anders Jensen-Urstad, Walter van Holst, Maddalena Falzoni, Hanno “Rince” Wagner, Piksel), external contributions (Gordon Lennox, Achim Klabunde, Laura Kalbag, Aral Balkan), and the crucial help of Sid Rao, Public Interest Technologist and ex-Ford-Mozilla Fellow at EDRi. Special thanks to Joe McNamee who had the original idea for this booklet and steered the process to a successful conclusion.*

*Project coordination: Guillermo Peris, Community Coordinator, EDRi*
*Layout: Heini Järvinen, Senior Communications Manager, EDRi*

*Distributed under a Creative Commons 4.0 Licence:*
[http://creativecommons.org/licenses/by-nc-sa/4.0/](http://creativecommons.org/licenses/by-nc-sa/4.0/)

*Printed thanks to the support of the European Cultural Foundation*

![European Cultural Foundation Logo](./european-cultural-foundation.jpg)

