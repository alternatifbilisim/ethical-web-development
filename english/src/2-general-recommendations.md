# 2. General recommendations

- Allow as much data processing on an individual’s device as possible.
- Where you must deal with user data, **use encryption**. Facilitate end-to-end encryption for all relevant communications. The aim there is that you simply cannot see private information.
- Where possible also **use data minimisation methods** – only ever process data that actually needs to be processed.
- The best solution is often to **use first-party resources** (i.e. your resources that you host) and **avoid third-party solutions** as much as possible. In other words, try to host everything on your own server. This includes third party code and content such as:

  - Cookies
  - CSS files
  - Images
  - Media such as video files, audio files
  - JavaScript, if you decide to use it
  - Frames with third-party content
  - Font files, if you need them

- If downloading a resource, such as a JavaScript or font file, is not allowed by the terms of its provider, then they may not be privacy-friendly and should therefore be avoided. If you do have to load third-party resources, then the use of **Subresource Integrity (SRI)** can be a good idea.
