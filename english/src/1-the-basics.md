# 1. The Basics

## EXPECTATIONS

People who visit a website have a range of legitimate expectations.

They want to be safe. They do not want any harm to come to their own equipment or the data on their equipment. They want their privacy to be respected. Some of these requirements are codified in legislation such as the EU’s General Data Protection Regulation (GDPR).

Previously, this was simply about a service being technically simple and reliable. But a service that is a bit broken, a bit clunky, a bit unpredictable, means that users have to accept that it does not meet their general expectations. They may not continue to use the site. More importantly in this context, however, if they decide to continue using the service, they will learn to tolerate things that maybe should not be tolerated and that are really signs of significant problems.


## TRUSTWORTHINESS AND SECURITY
In the current social and political climate, many are also concerned about correctness and truthfulness. How readily should a user trust software downloaded from a site? How readily should they trust links provided on a site?

Finally there is the old catchall: security. If safety is about trying to make sure a system does not do harm, security is about protecting a system from harm, protecting a system from accidents and attacks. So we have the triad: availability, integrity and confidentiality. Systems, services and data need to remain available despite component failures and distributed denial of service (DDoS) and other attacks. Measures need to be taken to preserve service and data integrity. Is a damaged database really still useful as a database? Data should of course only be visible to those who have the appropriate rights.

## PERSONAL DATA AND COMPLIANCE WITH THE GDPR

Organisations in (or targeting the citizens of) the European Union have to comply with the European Union’s General Data Protection Regulation (GDPR). This Regulation on data protection and privacy for all individuals in the EU (or by EU companies) aims primarily to give control to individuals over their personal data and simplifies the regulatory environment. It also addresses the export of personal data to jurisdictions outside the EU.

The GDPR is however a complex piece of legislation. There are many companies offering consultancy, training courses, interpretive guides and checklists. In addition, each EU Member State has its own data protection authority which has responsibilities when it comes to interpretation and enforcement.

Personal Data (also referred to, with somewhat different meaning, as Personal Identifiable Information or PII, in the United States) is any information relating to an identified or identifiable natural person (‘data subject’). An identifiable natural person is one who can be identified, directly or indirectly, in particular by reference to an identifier such as a name, an identification number, location data, an online identifier or to one or more factors specific to the physical, physiological, genetic, mental, economic, cultural or social identity of that natural person.

According to the GDPR, an IP address is personal data. This is because an IP address may be connected to an individual if it is combined with other information. When third-party solutions are used to provide functionality or content such as JavaScript and images, a website’s visitor’s IP address may be available to those third party providers. This is why, under the GDPR, a data processing agreement with each of those third party providers may be needed.Privacy by default means that, where you provide a service with a variety of levels of data processing, the default setting should be the one that most respects privacy. The user can then be given the option to change this setting, should they wish. This needs to be done in the most user-friendly way possible.

In any case, data subjects retain rights over their personal data and organisations have specific responsibilities in the event of security incidents or data breaches.

GDPR compliance is therefore a specialist area on its own. However, following the principles in this document will be a step towards legal compliance as well as ensuring an ethical approach. In addition, there are plenty of genuinely free resources available to help with ensuring compliance – which is good for you and good for your website visitors. See, for example, the European Data Protection Supervisor’s guidelines for web services:

[https://edps.europa.eu/data-protection/our-work/publications/guidelines/web-services_en](https://edps.europa.eu/data-protection/our-work/publications/guidelines/web-services_en)
