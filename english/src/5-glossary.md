# 5. Glossary

**CMS (CONTENT MANAGEMENT SYSTEM):** this kind of system supports the creation and management of digital content.

**CSP (CONTENT SECURITY POLICY):** computer security standard introduced to prevent cross-site scripting (XSS), clickjacking and other code injection attacks resulting from execution of malicious content in the trusted web page context.

**CRONJOB:** is a task launched by the software utility Cron which is a time-based job scheduler in Unix-like computer operating systems. People who set up and maintain software environments use Cron to schedule jobs (commands or shell scripts) to run periodically at fixed times, dates, or intervals. It typically automates system maintenance or administration.

**DDOS (DISTRIBUTED DENIAL OF SERVICE ATTACK):** an attack in which the perpetrator seeks to make network service unavailable to its intended users by temporarily or indefinitely disrupting one or more hosts. This is typically done by flooding the targeted environment with requests or other specific forms of traffic from many different sources in an attempt to overload the system and prevent some or all legitimate communication.

**END-TO-END ENCRYPTION:** system of communication where only the communicating parties can access the content. In principle, it prevents potential eavesdroppers – including telecom providers, internet access providers, transit providers and even the provider of the communication service – from being able to decrypt the content of the communication. End-to-end may imply user-to-service or user-to-user.

**FLOSS (FREE LIBRE OPEN SOFTWARE):** software freely licensed to be used, copied, studied, and changed in any way, and the source code openly shared so that people are encouraged to improve the design of the software. This is in contrast to proprietary software, where the software is under restrictive licensing and the source code is usually hidden from the users. There are also restrictions related to open-source software. These are necessary to preserve its fundamental nature as being free. Open source software also depends on an active and committed community to develop and maintain it.

**GDPR (GENERAL DATA PROTECTION REGULATION OF THE EUROPEAN UNION):** an EU Regulation on data protection that covers all individuals in the EU and that aims primarily to give control to individuals over their personal data and to simplify the regulatory environment. It also addresses the export of personal data to jurisdictions outside the EU.

**INTEROPERABILITY:** characteristic of a product or system, whose interfaces are designed to work with other products or systems, in either implementation or access, without significant restrictions.

**NGO:** Non Governmental Organisation.

**PERSONAL DATA:** any information relating to an identified or identifiable natural person (‘data subject’); an identifiable natural person is one who can be identified, directly or indirectly, in particular by reference to an identifier such as a name, an identification number, location data, an online identifier or to one or more factors specific to the physical, physiological, genetic, mental, economic, cultural or social identity of that natural person. (As defined by the GDPR).

**PEER-TO-PEER (P2P):** a distributed application architecture that divides tasks or workloads or indeed data between peers. Peers are equally privileged participants in the application. They are said to form a peer-to-peer network of nodes.

**PRIVACY BY DEFAULT:** the principle according to which an organisation (the data controller) ensures that only data strictly necessary for each specific purpose of the processing are processed by default, i.e. the most privacy protective setting is the default, even if the user has the option to choose less protective settings.

**STOPGAP:** temporary measure or short-term fix used until something better can be obtained.

**THIRD-PARTY CONTENT:** is at its simplest where the property rights related to some content are held by another party or dynamically imported from their equipment.

**TOR:** free software initially developed by the US military for enabling anonymous communication. Tor directs Internet traffic through a free, worldwide, volunteer overlay network consisting of more than seven thousand relays to conceal a user’s location and usage from anyone conducting network surveillance or traffic analysis. The Tor Browser automatically starts Tor background processes and routes traffic through the Tor network.