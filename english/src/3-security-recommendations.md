# 3. Security recommendations

Security is a process, and it needs to take into consideration the entire stack. Security decisions need to take account of differing, potentially contradictory, objectives, and they cannot be taken in isolation. Security related decisions may affect such features as the speed and user-friendliness of a service and have a significant impact on the resources needed, i.e. money and working time spent.

Generally speaking, you ought to be able to achieve the following:

- the site ought not to be a source of harm for the rest of the internet;
- the integrity of the site itself should be maintained; 
- all communications should be secure; 
- the security and privacy of visitors should be protected.

Achieving these objectives tends to rest on two pillars: sharing and standards. As threats and responses change it is important to share information, such as via specialist mailing lists. At the very least one should be aware of one’s local Computer Emergency Response Team (CERT) / Computer Emergency Readiness Team and Computer Security Incident Response Team (CSIRT). In addition, adopting appropriate current standards ensures that you are not the weak link in any chain.

## ISO
ISO has published a variety of standards which could be of interest, including the following on quality and security:

- ISO / IEC / IEEE 90003:2018 Software engineering
- ISO / IEC 27000 family of Information security standards

These standards provide, among other things, a common vocabulary which can be very useful. One place they can be useful is in demonstrating GDPR compliance.

## DNSSEC
Authenticating responses to DNS queries regarding your domain name is a good thing.

## TOR
In order to protect their privacy and security, people may prefer to use the Tor Browser. When choosing a hosting provider for your application, make sure Tor connections are allowed. For example, using the **Onionshare** tool, one can easily publish an anonymous and uncensorable version of the exisiting website with only a few clicks.

[https://onionshare.org/](https://onionshare.org/)


## HTTPS
To avoid middleman attacks and eavesdropping, do not allow unencrypted connections to your website and always encourage connection through **HTTPS**. The principal motivation for HTTPS is authentication of the accessed website and protection of the privacy and integrity of the exchanged data. Consider using **HSTS – HTTP Strict Transport Security** - to enforce this.

If you have root access on the server – it is your server - you can use Let’s Encrypt and get a certificate for all the domains you require. If you have shared hosting, choose a provider that offers the option to have a certificate provided by Let’s Encrypt.

[https://letsencrypt.org/](https://letsencrypt.org/)

When you use https, make sure that you allow only secure encryption methods. Some of the older https versions are no longer secure because they use outdated encryption. There are free resources on the web for testing the security of your web server, such as:

[https://www.ssllabs.com/ssltest/](https://www.ssllabs.com/ssltest/)


## CSP
Implement **Content Security Policy (CSP)**. This is a Worldwide Web Consortium (W3C) security layer that you can enable on your web server, in order to block external resources, such as scripts, style sheets, and images, from being loaded on your website. CSP helps to detect and mitigate certain types of attacks, including Cross Site Scripting (XSS) and data injection attacks. These attacks are used for everything from data access to site defacement to distribution of malware. You can customise the rules of your policy, white-listing some URLs.

If you do not have access to the web server configuration, you can still enable a CSP through an **.htaccess*** file or directly in the header of your website.You can test your configuration of CSP through a free service provided by Mozilla:

[https://observatory.mozilla.org/](https://observatory.mozilla.org/)


## JAVASCRIPT
There is a view that JavaScript ought to be avoided because it may exclude some users or may result in less accessibility for some users. However, JavaScript in and of itself does not mean content necessarily becomes inaccessible. Care is, however, required.

[https://webaim.org/techniques/javascript/](https://webaim.org/techniques/javascript/)

Implementing JavaScript tends to imply needing to build a non-JavaScript version. The best suggestion is to design a website without a JavaScript requirement, and build JavaScript-based features on top of it. This can make the browsing experience much more accessible for every user. Include a ```<noscript>``` tag to define alternate content that offers a version for users who have disabled JavaScript in their browser or who have a browser that still does not support JavaScript.


## SECURING SENSITIVE DATA
If the website stores personal data of its users, for example by allowing them to have their personal accounts with login:

- Enforce the usage of strong passwords;
- Never store passwords and other sensitive information in plaintext. Always use hashing and encryption of sensitive data at rest;
- Support two-factor authentication (2FA);
- If the website administrative capacities are limited, you can offload the above tasks to an identity provider and enable single sign on. Please remember that while choosing an identity provider, this may have privacy costs.

If personalised accounts are not needed, in some cases, commenting on blogposts could be a desired feature. Integrate with third-party open source plugins such as **Discourse** or the **Coral** project:

[https://www.discourse.org/](https://www.discourse.org/)

[https://coralproject.net/](https://coralproject.net/)

For additional details, refer to

[https://darekkay.com/blog/static-site-comments/](https://darekkay.com/blog/static-site-comments/)


## PROTECTION AGAINST DDOS ATTACKS
Distributed denial of service attacks (DDoS) are attacks typically accomplished by flooding the targeted machine with requests from many different sources, in an attempt to overload systems. If you are building an application for an NGO, an activist initiative or a civil society group, consider protecting it against DDoS. Some DDoS mitigation techniques use a third party that operates between a website’s visitor and the website’s hosting provider, acting as a reverse proxy for websites.  However, some DDoS mitigation techniques will not require the services of a third party service, but can handle the attack on a network level.

It may be worthwhile looking at **Deflect**, which is a free Open Source solution. It is developed by a digital security non-profit organisation.

[https://docs.deflect.ca/en/latest/about_deflect.html](https://docs.deflect.ca/en/latest/about_deflect.html)

However all DDoS mitigation services mean handing over control of your traffic to a third-party, whether or not this is done very quickly or the service is in-line all the time. The risks, reputational and otherwise, therefore need to be carefully evaluated.


## STATIC WEBSITES ARE BACK
Do you really need a dynamic website? If you do not require a database, chances are that current web standards such as HTML5 allow you to create a state-of-the-art website using static resources (HTML, CSS and perhaps JavaScript and font files) only.

This does not mean that you have to hand-code navigation bars etc. Modern static site generators such as Jekyll (made popular by GitHub Pages), Hugo or Pelican let you create your websites in Markdown or basic HTML and convert them into full, interlinked HTML for you. If you are using GitLab Pages, conversion and deployment can be automated easily.

Static websites achieve unrivaled uptimes because they do not need to connect to a database. All they need is a plain web server to serve the static files. Consequently they are less ridden by security gaps, and they are lightning fast.

