# 1. Giriş

## BEKLENTİLER

Bir web sitesinin ziyaretçileri, bir dizi haklı beklentilere sahiptir.

Güvende olmak isterler. Cihazlarına ya da cihazları üzerindeki bilgilere bir zarar gelmesini istemezler. Gizliliklerine saygı duyulmasını isterler. Bu gerekliliklerden bazıları AB’nin Genel Veri Koruma Tüzüğü (GDPR) gibi yasal düzenlemelerde ele alınmıştır. 

Daha önceleri, bu mesele teknik olarak yalın ve güvenilir bir hizmet olmaktan ibaretti. Ama bir miktar bozuk, kullanışsız ve öngörülemeyen bir hizmet, kullanıcıların, bu hizmetin temel beklentileri karşılamayacağını kabul etmek zorunda oldukları anlamına gelir. Siteyi kullanmaya devam etmeyebilirler. Ama bu durumda daha da önemlisi, eğer hizmeti kullanmaya devam etmeye karar verirlerse, hoş görmemeleri gereken önemli problemleri idare etmek zorunda kalacaklardır.


## GÜVENİRLİK VE GÜVENLİK
Mevcut sosyal ve siyasal iklimde, çoğu insan doğruluk ve güvenirlik hakkında endişeli. Bir kullanıcı siteden indirilen bir yazılıma ne kadar güvenmelidir? Site üzerinde sağlanan linklere ne kadar güvenmelidir?

Ve her daim genelgeçerimiz: güvenlik. Eğer “emniyet”, bir sistemin zarar vermeyeceğinden emin olmaksa, “güvenlik” de sistemi kaza veya saldırı sonucu zarara uğramaktan korumaktır. Yani elimizdeki üçlü takım şunlardan oluşur: erişilebilirlik, bütünlük ve gizlilik. Sistem bileşenlerinin hatalarına, dağıtık hizmet engelleme (DDoS) veya başkaca saldırılara rağmen sistemler, servisler ve veriler erişilebilir olmalıdırlar. Hizmet ve veri bütünlüğünü korumak için önlemler alınmalıdır. Hasar görmüş bir veritabanı, bir veritabanı olarak hala kullanışlı mıdır? Veriler tabii ki sadece gerekli hakları olan kişilere görünür olmalıdır.


## KİŞİSEL VERİLER VE GENEL VERİ KORUMA TÜZÜĞÜ’NE UYUMLULUK

Avrupa Birliği’ndeki kuruluşlar (ya da hedef kitlesi AB vatandaşları olan kuruluşlar) Avrupa Birliği’nin Genel Veri Koruma Yönetmeliği’ne (GDPR) uymak zorundadır. AB içerisindeki bütün bireylerin (veya AB şirketlerinin) verilerinin korunması ve kişisel gizlilikleri üzerine olan bu yönetmelik, bireylere kendi verilerinin üzerinde kontrol hakkı vermeyi ve bu kontrolün yapılacağı ortamı basitleştirmeyi amaçlar. Ayrıca kişisel verilerin AB dışına aktarılmasıyla da ilgilenir.

Fakat GDPR karmaşık bir yönetmeliktir. Bu konuda danışmanlık, eğitim, yorumlama rehberleri ve kontrol listesi hizmetleri sunan birçok şirket vardır. Buna ek olarak, AB üyesi her ülke kendisine ait, yorumlama ve yaptırım konularında sorumluluk sahibi veri koruma kurullarına sahiptir.

Kişisel Veriler (ayrıca ABD’de, biraz farklı anlam taşıyan, PII olarak anılan Kişisel Olarak Tanımlanabilir Bilgiler) kimliği belirlenmiş veya belirlenebilecek gerçek kişi (‘veri öznesi’) hakkında herhangi bir bilgi demektir. Kimliği belirlenebilir gerçek kişi, dolaylı veya dolaysız olarak, bir isim, kimlik numarası, konum verisi, çevrimiçi bir belirteç veyahut daha çok fiziksel, psikolojik, genetik, zihinsel, ekonomik, kültürel veya sosyal kimliğine ait bir veya daha çok faktörle kimliği belirlenebilen kişidir.

GDPR’ye göre IP adresi kişisel bir veridir. Bunun sebebi, IP adresinin başka bilgilerle bağlantılı olduğunda bir insanla eşleştirilebilmesidir. Bazı işlevler için kullanılan üçüncü parti çözümler veyahut üçüncü taraflarca kullanılan Javascript veya görsel içerikler, web site ziyaretçesinin IP adresini o üçüncü parti sağlayıcılara açık hale getirebilir. Bu sebeple, GDPR nazarında, üçüncü parti sağlayıcılarının her biriyle veri işleme anlaşması yapmak gerekebilir. Varsayılan gizlilik, çeşitli düzeylerde veri işlediğiniz bir hizmet sağlıyorsanız, o hizmetin varsayılan ayarlarının gizliliğe en çok önem veren ayar olması anlamına gelir. Kullanıcıya bu ayarı istedikleri takdirde değiştirme seçeneği sunulabilir. Bunun olabilecek en kullanıcı dostu biçimde yapılması gerekir.

Her halükârda, veri özneleri kendi verileri üzerinde hak sahibidir. Ve hizmet veren kuruluşların, güvenlik kazaları veya veri sızıntısı olduğunda belirli sorumlulukları vardır.

Bu sebeple GDPR’a uyumluluk uzmanlık gerektiren bir alandır. Yine de, bu metindeki prensiplere sadık kalmak yasal uyumluluğa doğru bir adım atmak anlamına geldiği gibi etik olarak da uygun bir yaklaşım olacaktır. Buna ek olarak, birçok ücretsiz kaynak, sizin için ve web sitenizin ziyaretçileri için iyi bir şey olan uyumluluğu güvene almak için erişime hazırdır. Örneğin Avrupa Veri Koruma Denetmeninin web hizmetleri rehberine bakabilirsiniz:

[https://edps.europa.eu/data-protection/our-work/publications/guidelines/web-services_en](https://edps.europa.eu/data-protection/our-work/publications/guidelines/web-services_en)
