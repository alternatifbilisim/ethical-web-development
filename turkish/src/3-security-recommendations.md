# 3. Güvenlik Önerileri

Güvenlik bir süreçtir ve her şeyi kapsamak zorundadır. Güvenlik kararları alınırken, değişen ve potansiyel olarak çelişebilen hedefler dikkate alınmalı, bütüncül bir yaklaşım sergilenmelidir. Güvenliğe dair kararlar, bir servisin hızı ve kullanıcı dostu olması gibi özelliklerini etkileyebilir ve ihtiyaç duyulan kaynaklar -yani harcanan para ve emek zamanı- konusunda büyük etkileri olabilir.

Genel olarak, şunları başarabilmelisiniz:

  - Siteniz internetin geri kalanı için zarar kaynağı olmamalı,
  - Sitenizin bütünlüğü korunabilmeli,
  - Tüm iletişim güvenli olmalı,
  - Ziyaretçilerin güvenliği ve gizliliği korunmalı,

Bu hedeflere ulaşmak genellikle şu iki ayak üzerine kuruludur: Paylaşım ve standartlar. Tehditler ve önlemler değiştikçe konuyla ilgili e-posta listeleri gibi kanallarda bilgi paylaşımı önem kazanır. En azından kullanıcının, yerel Bilgisayar Acil Müdahale Ekibini (CERT) / Bilgisayar Acil Hazırlık Ekibini ve Bilgisayar Güvenlik Arıza Müdahale Ekibini (CSIRT) tanıması gerekir. Ayrıca uygun ve güncel ölçütleri sağlayarak zincirin en zayıf halkası olmaktan kaçınır.


## ISO
ISO, ilginizi çekebilecek çeşitli ölçütler yayınlamış durumda. Bu ölçütlere kalite ve güvenlik alanındaki şunlar da dahildir:

  - ISO / IEC / IEEE 90003:2018 Yazılım mühendisliği
  - ISO / IEC 27000 bilgi güvenliği ölçütleri 

Bu ölçütler başka bir çok şeyin yanında çok kullanışlı olabilen ortak bir kelime dağarcığı da paylaşır. Kullanışlı olabilecekleri bir yer de GDPR uyumluluğudur. 


## DNSSEC
Alanadınız için yapılan DNS sorgularına verilecek yanıtları doğrulamak iyi bir şeydir.



## TOR
İnsanlar, gizliliklerini ve güvenliklerini korumak için Tor Browser kullanmayı tercih edebilir. Uygulamanıza barındırma hizmeti seçerken, Tor bağlantılarına izin verildiğinden emin olun. Örneğin, Onionshare aracı kullanılarak, var olan bir web sitesinin anonim ve sansürlenemeyen versiyonu sadece birkaç tık ile kolayca yayınlanabilir.

[https://onionshare.org/](https://onionshare.org/)


## HTTPS
Aradaki adam (man in the middle) saldırılarından ve gizli dinlemelerden kaçınmak için web sitenizde şifrelenmemiş bağlantılara izin vermeyin ve her zaman **HTTPS** yoluyla bağlantı kurmayı destekleyin. HTTPS kullanmanın en temel motivasyonu ulaşılan web sitesinin kimliğinin doğrulanması, gizliliğin korunması ve alışverişi yapılan verinin bütünlüğüdür. Daha zorlayıcı olmak için, **HSTS (HTTP Katı Aktarım Güvenliği)** kullanmayı düşünebilirsiniz.

Eğer sunucuya yönetici erişimine sahipseniz, eğer sizin sunucunuzsa, Let’s Encrypt kullanabilir ve ihtiyacınız olan tüm alan adları için sertifika edinebilirsiniz. Paylaşımlı barındırmanız varsa, Let’s Encrypt sertifikalarını destekleyen sağlayıcıyı seçin.

[https://letsencrypt.org/](https://letsencrypt.org/)

HTTPS kullandığınız zaman sadece güvenli şifreleme yöntemlerine izin verdiğinizden emin olun. Bazı eski HTTPS versiyonları zamanı geçmiş şifrelemeleri sebebiyle artık güvenli değillerdir. İnternette web sunucunuzu test edebilmeniz için ücretsiz kaynaklar var. Örneğin:

[https://www.ssllabs.com/ssltest/](https://www.ssllabs.com/ssltest/)



## CSP
İçerik Güvenliği Politikası (CSP) uygulayın. Bu, Dünya Çapında Ağ Konsorsiyumu'nun (W3C) bir güvenlik katmanıdır ve bunu betikler (scripts), biçem sayfaları (style sheets) ve görseller gibi dış kaynakların web sitenizden yüklenmesini engellemek için web sunucunuzda aktif hale getirebilirsiniz. CSP size, Siteler Arası Betik Çalıştırma (XSS) ve veri enjeksiyonu dahil, belirli saldırı çeşitlerini tespit etmeniz ve verilen zararı hafifletmeniz için yardım sağlar. Bu saldırılar veri erişiminden site içeriğini bozmaya ve kötü amaçlı yazılım dağıtımına kadar her şey için yapılır. Politika kurallarını istediğiniz URL’leri izinliler listesine alarak özelleştirebilirsiniz.

Eğer web sunucusu yapılandırmasına erişiminiz yoksa bile .htaccess dosyasıyla veya direkt web sitenizin başlıkları (header) ile CSP'yi etkinleştirebilirsiniz.

Mozilla tarafından ücretsiz sağlanan bir hizmetle CSP yapılandırmanızı test edebilirsiniz:

[https://observatory.mozilla.org/](https://observatory.mozilla.org/)


## JAVASCRIPT
JavaScript’ten kaçınılması gerektiği, çünkü bazı kullanıcılar için dışlayıcı olduğu veya bazı kullanıcıların erişim seviyesini düşürdüğü yönünde bir görüş var. Fakat başlı başına Javascript kullanmak, içeriklerin erişilmez hale geleceği anlamına gelmez. Yine de kullanmanın bazı gereklilikleri vardır.

[https://webaim.org/techniques/javascript/](https://webaim.org/techniques/javascript/)

JavaScript uygulamak genellikle yanında JavaScript’siz bir versiyon yaratma gerekliliğini beraberinde getirir. En iyisi JavaScript zorunluluğu olmayan bir web sitesi tasarlamak ve JavaScript temelli özellikleri bunun üzerine kurmaktır. Böylelikle her kullanıcı için erişilebilir bir gezinti (browsing) deneyimi sağlanabilir. Bir ```<noscript>``` etiketi kullanarak, JavaScript’i tarayıcısında etkinleştirmemeyi tercih eden kullanıcılar ve JavaScript desteklemeyen tarayıcı kullananlar için alternatif içerik tanımlayın.


## HASSAS VERİLERİ GÜVENCE ALTINA ALMAK
Kullanıcılarının kişisel verilerini depolayan bir web siteniz varsa, örneğin kişisel hesaplarıyla oturum açıyorlarsa:

- Güçlü parola kullanımını zorunlu kılın;
- Parola ve diğer hassas bilgileri asla düz metin saklamayın. Saklanan hassas verileri her zaman şifreleyin ya da hashing kullanın;
- İki faktörlü kimlik doğrulamasını (2FA) destekleyin;
- Eğer web sitesinin yönetici kapasitesi sınırlıysa yukarıdaki görevlerin yükünü bir kimlik sağlayıcısına (identity provider) verin ve tek girişle oturum açmayı etkinleştirin. Lütfen kimlik sağlayıcısı seçerken bunun gizlilikten ödün anlamına gelebileceğini unutmayın.

Eğer sitenizde kişisel hesaplar gerekli değilse, ve sadece blog gönderilerine yorum yapabilme özelliğini arzu ediyorsanız **Discourse** veya **Coral Project** gibi üçüncü parti açık kaynak eklentileri kullanabilirsiniz:

[https://www.discourse.org/](https://www.discourse.org/)

[https://coralproject.net/](https://coralproject.net/)

Daha fazla bilgi için:

[https://darekkay.com/blog/static-site-comments/](https://darekkay.com/blog/static-site-comments/)


## DDOS SALDIRILARINA KARŞI KORUMA
Dağıtık Hizmet Engelleme (DDoS) saldırıları, genellikle hedef makineyi yoğunluktan çalışamaz hale getirmek niyetiyle birçok kaynaktan aşırı sayıda yapılan istekler ile gerçekleştirilir. Eğer bir STK için (aktivist girişim veya sivil toplum grubu) uygulama yazıyorsanız DDoS saldırılarına karşı korumayı göz önünde bulundurun. Bazı DDoS mücadele teknikleri, web sitesinin ziyaretçisi ve barındırma sağlayıcısı arasında, ters vekil (reverse proxy) görevinde üçüncü parti bir servis kullanır. Fakat bazı DDoS ile mücadele teknikleri üçüncü parti kullanımını gerektirmeyerek ağ düzeyinde saldırıyı karşılayabilir.

**Deflect**’i incelemek işinize yarayabilir. Deflect ücretsiz bir açık kaynak kodlu çözümdür. Kâr amacı gütmeyen bir dijital güvenlik kuruluşu tarafından geliştirilmiştir.

[https://docs.deflect.ca/en/latest/about_deflect.html](https://docs.deflect.ca/en/latest/about_deflect.html)

Fakat bütün DDoS ile mücadele hizmetleri, trafiğinizin kontrolünü bir üçüncü partiye devretmeniz anlamına gelir; bunun farkedilemeyecek kadar hızlı olup olmaması veya hizmetin sürekli kontrolde olup olmaması fark etmez. Bu sebeple, itibar veya başka türlü riskler dikkatlice değerlendirilmelidir.


## STATİK WEBSİTELERİ GERİ DÖNDÜ
Gerçekten dinamik web sitesine ihtiyacınız var mı? Eğer veri tabanı ihtiyacınız yoksa, HTML5 gibi modern web standartlar, size sadece statik kaynaklar (HTML, CSS ve belki sadece JavaScript ve font dosyaları) kullanarak son moda web sitesi yaratma imkanı sağlar.

Bu, dolaşım menüleri ve benzeri yapıları kendi elinizle yapmanız anlamına gelmez. Jekyll (GitHub Pages popülerleşti), Hugo veya Pelican gibi statik site üreticileri size Markdown veya basit HTML kullanarak web siteleri yaratma olanağı verir ve onları sizin için tümüyle birbirleri ile bağlantılandırılmış HTML'e dönüştürür. Eğer GitLab Pages kullanıyorsanız dönüştürme ve dağıtım kolayca otomatikleştirilebilir.

Statik web siteleri, veri tabanlarına bağlanmaları gerekmediği için eşsiz erişilebilirlik zamanlarına ulaşabilir. Tek ihtiyaçları olan şey, statik dosyaları sunmaları gereken basit bir web sunucusudur. Sonuç olarak, güvenlik sorunlarından daha az etkilenirler ve çok hızlıdırlar.
