# 2. Genel Öneriler

- Veri işlemeyi, mümkün olduğu kadar bireylerin kendi cihazları üzerinde yapmasını sağlayın.

- Kullanıcı verisiyle iş yapacağınız zaman **şifreleme kullanın**. İlgili tüm iletişimler için uçtan uca (end-to-end) şifreleme sağlayın. Bunun amacı kişisel verilerin kolayca görünmemesidir.

- Mümkün olduğunda **veri azaltma yöntemleri kullanın**. Sadece işlem görmesi gereken veriyi işleyin.

- En iyi çözümler sık sık **birinci parti kaynak kullanmak** (yani sizin yönettiğiniz kaynaklar) ve olabildiği kadar üçüncü parti çözümlerden kaçınmaktır. Bir başka deyişle, her şeyi kendi sunucunuzda yönetmeye çalışın. Buna üçüncü parti kod ve içerikler de dahildir. Örneğin:

  - Çerezler (Cookies)
  - CSS dosyaları
  - Görseller
  - Görüntü ve ses dosyaları gibi medyalar
  - JavaScript (kullanmayı tercih ediyorsanız)
  - Üçüncü parti içeriğe sahip çerçeve (frame / iframe)
  - Yazı tipi dosyaları (İhtiyaç duyarsanız)

- Eğer JavaScript veya yazı tipi dosyası gibi bir kaynağın indirilmesi sağlayıcısı tarafından engelleniyor, üçüncü parti şeklinde kullanılmaya zorlanıyorsa, o sağlayıcılar gizlilik dostu değildir ve onlardan kaçınılmalıdır. Eğer üçüncü parti kaynakları yüklemeniz gerekirse, **Alt-Kaynak Bütünlüğü (Kıs.: SRI; İng.: Subresource Integrity)** kullanımı iyi bir fikir olabilir. Detaylar için: [https://developer.mozilla.org/en-US/docs/Web/Security/Subresource_Integrity](https://developer.mozilla.org/en-US/docs/Web/Security/Subresource_Integrity)
