# 5. Sözlük

**CMS (İÇERİK YÖNETİMİ SİSTEMİ):** Dijital içeriğin oluşturulmasını ve yönetilmesini sağlayan bir sistemdir. 

**CSP (İÇERİK GÜVENLİĞİ POLİTİKASI):** Güvenilir web sayfalarında kötü amaçlı içeriğin yürütülmesi ile sonuçlanan, siteler arası betik çalıştırma (XSS), clickjacking (sahte butonlara tıklama) ve diğer kod enjeksiyon saldırılarını önlemek için tanıtılan bilgisayar güvenlik standardı.

**CRONJOB:** Unix benzeri işletim sistemlerinde bir görev zamanlayıcısı olan Cron yazılımı tarafından başlatılan bir görevdir. Yazılım ortamlarını kuran ve bakımını yapan kişiler, belirli zamanlarda, tarihlerde veya aralıklarla periyodik olarak çalışmak üzere işleri (komutlar veya kabuk betikleri) planlamak için Cron'u kullanır. Genellikle sistem bakımını veya yönetimini otomatikleştirir.

**DDoS (DAĞITIK HİZMET ENGELLEME):** Failin, bir veya daha fazla sunucuyu, geçici veya süresiz olarak bozarak ağ hizmetini hedeflenen kullanıcılar için kullanılamaz hale getirmeye çalıştığı bir saldırı. Bu tipik olarak, olağan iletişimin bir kısmının veya tamamının önlenmesi amacıyla, hedeflenen ortamın birçok kaynaktan gelen istekler veya özel tip trafik ile doldurup, sistemin yoğunluktan çalışamaz hale getirilmesiyle yapılır.

**UÇTAN UCA ŞİFRELEME:** Sadece iletişim kuran tarafların içeriğe ulaşabildiği iletişim sistemi. Prensip olarak, telekom sağlayıcıları, internet erişim sağlayıcıları, transit sağlayıcıları ve hatta iletişim hizmeti sağlayıcısı da dahil olmak üzere potansiyel gizli dinleme cihazlarının iletişimin şifresini çözmesini önler. Uçtan uca, kullanıcıdan hizmete veya kullanıcıdan kullanıcıya anlamına gelebilir.

**FLOSS (FREE LIBRE OPEN  SOFTWARE):** Herhangi bir amaçla kullanılmak, kopyalanmak, çalışmak ve değiştirilmek üzere serbestçe lisanslanan ve insanların yazılım tasarımını geliştirmeleri için kodları açık şekilde paylaşılan yazılımdır. Bu, yazılımın kısıtlayıcı lisanslama altında olduğu ve kaynak kodun genellikle kullanıcılardan gizlendiği sahipli yazılımın tersidir. Açık kaynaklı yazılımlarla ilgili kısıtlamalar da vardır. Bunlar, temel doğasının özgür kalması için gereklidir. Ayrıca, açık kaynaklı yazılımlar onu geliştirmek ve sürdürmek isteyen aktif ve kendini bu işe adamış bir topluluğa dayanır.

**GDPR (AVRUPA BİRLİĞİ GENEL VERİ KORUMA DÜZENLEMESİ):** AB'deki tüm bireyleri kapsayan ve öncelikli olarak bireylere kişisel verileri üzerinde kontrol sağlamayı ve düzenleyici ortamı basitleştirmeyi amaçlayan bir AB veri koruma düzenlemesidir. Ayrıca, kişisel verilerin AB dışındaki yargı bölgelerine aktarılmasını da ele almaktadır.

**BİRLİKTE İŞLERLİK:** Arayüzleri, önemli kısıtlamalar olmaksızın diğer ürünler veya sistemlerle birlikte çalışmak üzere tasarlanan bir ürün veya sistemin özelliği. 

**STK:** Sivil Toplum Kuruluşu.

**KİŞİSEL VERİ:** Kimliği belirlenmiş veya belirlenebilir gerçek bir kişiyle (‘veri öznesi’) ilgili herhangi bir bilgi. Kimliği belirlenebilir gerçek kişi, doğrudan veya dolaylı olarak, özellikle bir ad, bir kimlik numarası, konum verisi, çevrimiçi bir tanımlayıcı veya fiziksel, fizyolojik, genetik, zihinsel, ekonomik, kültürel veya sosyal kimliğini belirten bir referansla tanımlanabilen gerçek kişidir. (GDPR tarafından tanımlandığı şekliyle)

**PEER-TO-PEER (P2P):** Görevleri, iş yüklerini veya gerçekten verileri eşler arasında bölen dağıtık bir uygulama mimarisi. Eşler uygulamada eşit derecede ayrıcalığa sahip katılımcılardır. Eşlerin düğümlerden (node) oluşan bir peer-to-peer ağ oluşturdukları söylenir.

**VARSAYILAN GİZLİLİK:** Veri sorumlusu bir kuruluşun, varsayılan olarak, her bir belirli amaç için, işlenmesi gerekli en az verinin işlendiğinden emin olmasını sağlayan ilkedir. Yani kullanıcı daha az koruyucu ayarları seçme seçeneğine sahip olsa bile en koruyucu güvenlik ayarı varsayılandır.

**GEÇİCİ ÖNLEM (STOPGAP):** Daha iyi bir şey elde edilene kadar kullanılan geçici önlem veya kısa süreli düzeltme.

**ÜÇÜNCÜ PARTİ İÇERİĞİ:** En basitçe, mülkiyet haklarının başka bir tarafça tutulduğu veya bu tarafların ekipmanlarından dinamik olarak alındığı çeşitli içeriklerdir.

**TOR:** Başlangıçta anonim iletişimi sağlamak üzere ABD ordusu tarafından geliştirilen özgür yazılım. Tor internet trafiğini, herhangi bir kullanıcının yerini ve kullanımını ağ gözetimi veya trafik analizi yapanlardan gizlemek için, yedi binden fazla röleden (relay) oluşan ücretsiz, dünya çapında ve gönüllü bir ağ katmanı içerisinden yönlendirir. Tor Tarayıcı, Tor arkaplan işlemlerini otomatik olarak başlatır ve Tor ağı üzerinden trafiği yönlendirir.
