# Avrupa Dijital Haklar Örgütü - EDRi

![Edri Logo](./edri.jpg)

2002 yılında kurulan EDRi, çevrimiçi hakları ve özgürlükleri savunan en büyük Avrupa ağıdır.

Halen 42 sivil toplum kuruluşu EDRi üyesidir ve 30 gözlemci çalışmalarımıza yakından katkıda bulunmaktadır.

Misyonumuz, gizlilik, veri koruma ve ifade ve bilgi özgürlüğü dahil olmak üzere dijital ortamda insan haklarını ve hukukun üstünlüğünü teşvik etmek, korumak ve savunmaktır.

Vizyonumuz, devlet otoritelerinin ve özel şirketlerin çevrimiçi ortamda herkesin temel hak ve özgürlüklerine saygı duyduğu bir Avrupa içindir. Genel amacımız, sivil toplumun ve bireylerin haklarının kontrolü kendi ellerinde olacak şekilde teknolojik ilerlemeyi benimsemeleri için güçlendirildikleri yapıları inşa etmektir.


[![Edri Donate](./donate.jpg)](https://edri.org/donate)



KİTLE GÖZETİMİ.
KEYFİ SANSÜR.
İÇERİK KISITLAMALARI.

Şirketler ve hükümetler özgürlüklerimizi giderek kısıtlamaktadır.

ŞİMDİ bağış yap:
https://edri.org/donate 



GİZLİLİK!
İFADE ÖZGÜRLÜĞÜ!
BİLGİ VE KÜLTÜRE ERİŞİM!


Çevrimiçi hakları ve özgürlükleri savunuyoruz.
