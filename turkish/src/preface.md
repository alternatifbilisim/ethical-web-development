![Güvenlik aslında oldukça basittir. Karmaşık olan şey bir ihlalden sonraki temizliktir. Sitenizi ziyaret edenler saygıyı hak ediyor. Bu da onların bilgilerinin üçüncü şahıslara sızdırılmasına sebep olan özensizlikleri yapmamak ve yeterli güvenliği sağlamak anlamına geliyor. Sitenizi ziyaret edenlere saygı göstermek aynı zamanda veri koruma mevzuatlarına uymaya yönelik büyük bir adım atma anlamına da geliyor.](./preface-respect.jpg)


----------------


*Bu yayın, EDRi ağı uzmanlarının (Anders Jensen-Urstad, Walter van Holst, Maddalena Falzoni, Hanno “Rince” Wagner, Piksel), dış destekçilerin (Gordon Lennox, Achim Klabunde, Laura Kalbag, Aral Balkan) ve Public Interest Technology ve EDRi’de eski Ford-Mozilla üyesi Sid Rao’nun çok önemli katkılarıyla birlikte kapsamlı kolektif bir çalışmanın sonucudur. Bu kılavuz fikrinin sahibi ve süreci başarılı bir şekilde yöneten Joe McNamee’ye de özel teşekkürler..*

&nbsp; 

*Proje koordinasyonu: Guillermo Peris, Topluluk Koordinatörü, EDRi*

*Tasarım: Heini Järvinen, Kıdemli İletişim Müdürü, EDRi*

&nbsp; 

*Creative Commons 4.0 lisansıyla dağıtılır:*
[http://creativecommons.org/licenses/by-nc-sa/4.0/](http://creativecommons.org/licenses/by-nc-sa/4.0/)

&nbsp; 

*European Cultural Foundation (Avrupa Kültür Derneği) desteğiyle basılmıştır.*

![European Cultural Foundation Logo](./european-cultural-foundation.jpg)
