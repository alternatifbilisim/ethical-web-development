# 0. Sunuş

Bir web sitesi neredeyse bir canlı gibidir. Çoğunlukla, sitenin kendisi statik değildir ve dinamik özelliklerine ek olarak, sitenin çevresi de sürekli değişime uğrar. Bu durum da daha fazla değişikliğe yol açar.

Bir web sitesini ziyaret edenler de çok çeşitli olabilir. Kullandıkları teknolojiler ve uzmanlıkları oldukça çeşitlidir. 

Birçok web sitesi de çeşitli harici hizmetlere ve kaynaklara bağlıdır. Onlar da gelişmeye devam eder. 

Web geliştiricileri, kullanıcıların artan beklentileri ve birçok organizasyonun web site geliştirmeye ayırdığı sınırlı kaynak sorunuyla uğraşırken, dış kaynak ve servisler kullanma eğilimi artmaktadır.

Örneğin, site geliştiricilerinin, “bedava” olan yazı tipi ve betik gibi kaynakları almaları ve onları tasarladıkları web sitelerinde kullanmaları giderek daha da yaygınlaşmaktadır. Bunlar site geliştiriciler için “bedava” olsa da, kullanıcılar ve sitenin sahibi kuruluş için istenilmeyen yan etkileri olabilir. Örneğin, bazı kaynaklar ve servisler, özellikle de bilgi oburu internet şirketleri tarafından sağlananlar, kişisel veri gizliliğinin altını oyabilirler. Başka servisler ise güvenlik üzerine olumsuz etkilere sahip olabilir. Her iki durumda da site sahiplerinin itibarı zarar görebilir, hatta yasal zorluklarla bile karşılaşılabilir.

İşte buna dikkat etmek gerekir. Fakat bu sorunla ilgili farkındalık genel olarak azdır ve bu tür uygulamalar çoktan yaygınlaşmıştır bile. Bu metnin amacı, sorunları açıklamak ve mümkün olduğu kadar kullanışlı çözümler ortaya koymaktır.

Bu rehber, teknik konseptlere güçlü şekilde hâkim olan web geliştiricileri ve bakımcılarını hedeflemektedir. Dokümanı kısa ve öz tutup kullanılabilirliğini arttırmak adına, gerekli yerlerde arkaplan bilgileri için linkler yer almaktadır.

Umuyoruz ki, bu rehber geliştiricileri ve bakımcıları Web’i özüne; yani temel hakları, demokrasiyi ve ifade özgürlüğünü arttıran, merkezi olmayan bir araca, geri döndürmelerine yardımcı olacaktır.

Bu bizim de İnternetimiz. Hepimiz sorumluluk almalıyız.  
